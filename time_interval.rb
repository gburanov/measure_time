require_relative 'time_helpers'
require_relative 'settings'

class TimeInterval
  include TimeHelpers
  
  attr_accessor :start
  attr_accessor :end
  
  def initialize
    @valid = false
    @parts = 0
  end
  
  def add time
    if time.is_a? String
      time = to_datetime(time)
    end
    
    if @start.nil?
      @start = time
      @end = time
      @parts += 1
      return true
    end

    # check this time is within 2 hours with begin
    time_minus = substract_minutes(time, Settings.between_time)
    if (time_minus < @end)
      @end = time
      @parts += 1
      return true
    end
    
    return false
  end
  
  # two hour from begin
  def approximate_begin
    time_minus = substract_minutes(@start, Settings.first_time)
  end
  
  def approximate_end
    @end
  end
  
  def total_time
    ((approximate_end - approximate_begin) * 24 * 60).to_i
  end
  
  def to_s
    '[' + approximate_begin.to_s + "..." + approximate_end.to_s + "](" + total_time.to_s + " minutes)" +
    'consist of ' + @parts.to_s + " parts"
  end
  
  def date
    @start.to_date
  end
  
end