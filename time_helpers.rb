require 'time'
require 'active_support/all'

module TimeHelpers
  
  def to_datetime str
    # example
    # Wed Nov 26 12:44:59 2014 +0100
    DateTime.strptime(str, "%a %b %e %H:%M:%S %Y %Z")
  end
  
  def substract_hours(dt, n)
    dt.advance(:hours => -n)
  end

  def add_hours(dt, n)
    dt.advance(:hours => n)
  end
  
    def substract_minutes(dt, n)
    dt.advance(:minutes => -n)
  end

  def add_minutess(dt, n)
    dt.advance(:minutes => n)
  end
  
end