require_relative 'hash'

class IntervalStatistics
  
  def initialize type
    if type != :daily and type != :weekly and type != :monthly
      raise "Invalid type"
    end
    @type = type
  end
  
  def analyze(intervals)
    @time = Hash.new(0) # 0 - default value

    intervals.each do |interval|
      case @type
        when :daily
          key = interval.date.yday
        when :weekly
          key = interval.date.cweek
        when :monthly
          key = interval.date.month
      end
      @time[key] += interval.total_time
    end

    @time
  end


  def report
    case @type
      when :daily
        text = "Day"
      when :weekly
        text = "Week"
      when :monthly
        text = "Month"
    end


    puts "---------"
    puts text + " statistics"
    @time.each do |month|
      puts text + " " + month.first.to_s + " and time " + ProjectTime.new(month.second).to_s
    end
    generate_graph text
  end

  def generate_graph text
    unless File.directory?('images')
      FileUtils.mkdir_p('images')
    end

    file = 'images/' + text + '.png'
    puts "Saving to ... #{file}", file

    require 'gruff'
    g = Gruff::SideStackedBar.new
    g.title = text
    g.labels = @time.hmap do |k,v|
      { k.to_i => k.to_s }
    end
    g.data :Time, @time.values
    g.write(file)
  end
  
  
end