require_relative "../time_interval"
require "test/unit"

class TestTimeInterval < Test::Unit::TestCase
  
  def test_one_date
    ti = TimeInterval.new
    ret = ti.add("Wed Nov 26 12:44:59 2014 +0100")
    assert_equal true, ret
    interval = ti.total_time
    assert_equal interval, Settings.first_time
  end
  
  def test_some_close_dates
    ti = TimeInterval.new
    ret = ti.add("Wed Nov 26 12:44:59 2014 +0100")
    assert_equal true, ret
    ret = ti.add("Wed Nov 26 13:44:59 2014 +0100")
    assert_equal true, ret
    interval = ti.total_time
    assert_equal interval, Settings.first_time + 60 
  end
  
  def test_big_distance_between_dates
    ti = TimeInterval.new
    ret = ti.add("Wed Nov 26 12:44:59 2014 +0100")
    assert_equal true, ret
    ret = ti.add("Wed Nov 27 13:44:59 2014 +0100")
    assert_equal false, ret
  end
  
end