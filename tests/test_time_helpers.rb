require_relative "../time_helpers"
require "test/unit"


class TestTimeHelpers < Test::Unit::TestCase
  include TimeHelpers
  
  def test_to_str_1
    datetime = to_datetime("Wed Nov 26 12:44:59 2014 +0100")
    assert_equal 12, datetime.hour
    assert_equal 26, datetime.day
  end
  
  def test_to_str2
    datetime = to_datetime("Tue Nov 25 14:56:32 2014 +0300")
    assert_equal 14, datetime.hour
    assert_equal 25, datetime.day
  end
  
  def test_increment
    datetime = to_datetime("Wed Nov 26 12:44:59 2014 +0100")
    datetime = add_hours(datetime, 2)
    assert_equal 14, datetime.hour
  end
  
  def test_decrement
    datetime = to_datetime("Wed Nov 26 12:44:59 2014 +0100")
    datetime = substract_hours(datetime, 2)
    assert_equal 10, datetime.hour
  end
  
end