# README #
Very simple git log analyser

### What is this repository for? ###
* Try to measure how much time have you spent on this project.
* I understood too late that time management is critical for freelance developer
* So, I want to approximate time spent on my previous projects
* To check how good is this approximation method, you can try to run it on current projects and check how good is it
* To set it up, you can play with numbers in ''config/application.yml''

### How do I get set up? ###
* Run ''bundle install''
* Generate git log file ''git log > file_name_to_parse.txt''
* Run ''main file_name_to_parse.txt''
* Run unit test ''./run_tests.sh''

### Who do I talk to? ###
* In case of any problems, drop me a line gburanov@gmail.com