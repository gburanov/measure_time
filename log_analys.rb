require_relative 'git_log_parser'
require_relative 'time_interval'
require_relative 'project_time'
require_relative 'interval_statistics'

class LogAnalys
  
  def initialize file_name
    @log_parser = GitLogParser.new file_name
  end
  
  def start
    parse
    calculate
    report
  end
  
  private
  
  def parse
    logs = @log_parser.parse
    my_logs = logs.select do |log|
      author = log[:author]

      not author.nil? and author.include? Settings.email
    end
    
    @intervals = []
    time_interval = TimeInterval.new
    
    my_logs.each do |log|
      date = log[:date]
      
      ret = time_interval.add(date)
      if ret == false
        @intervals << time_interval
        time_interval = TimeInterval.new
        time_interval.add(date)
      end
    end
    @intervals << time_interval
  end

  def calculate
    # currently this involves lot of extra steps
    # but since all data we have is in memory, I dont care a lot
    @day_statistics = IntervalStatistics.new(:daily)
    @day_statistics.analyze(@intervals)

    @week_statistics = IntervalStatistics.new(:weekly)
    @week_statistics.analyze(@intervals)

    @month_statistics = IntervalStatistics.new(:monthly)
    @month_statistics.analyze(@intervals)

    @total_time = 0
    # measure total time
    @intervals.each do |interval|
      @total_time += interval.total_time
    end

  end
  
  def report
    @day_statistics.report
    @week_statistics.report
    @month_statistics.report

    puts "Total time " + ProjectTime.new(@total_time).to_s
  end
  
end