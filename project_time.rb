class ProjectTime
  
  def initialize minutes
    @minutes = minutes
  end
  
  def hours
    (@minutes / 60.0).round(2)
  end
  
  def money
    (hours * Settings.hour_price).round(2)
  end
  
  def to_s
    @minutes.to_s + " minutes, " + hours.to_s + " hours. Money is " + money.to_s
  end
  
end